// Copyright (c) 2017 Computer Vision Center (CVC) at the Universitat Autonoma de Barcelona (UAB). This work is licensed under the terms of the MIT license. For a copy, see <https://opensource.org/licenses/MIT>.


#include "Carla/iSIM/SceneSetter.h"
#include "Engine/Texture2D.h"
#include "Containers/UnrealString.h"
#include "IImageWrapper.h"
#include "IImageWrapperModule.h"
#include "EngineUtils.h"
#include "Engine/World.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Engine/GameEngine.h"


// Sets default values
ASceneSetter::ASceneSetter()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	this->cam_location = FVector(8030.0, -20130.0,  18970.0);
	this->ortho_width = 15000.0;
	this->scan_start_height = cam_location.Z;
	this->scan_to_Z = 10097.522461;
	this->texture_size = FVector2D(300, 300);
	
	this->tree_topleft = FVector2D(132,106);
	this->tree_bottomright = FVector2D(266, 219);
	this->ice_patch_center = ConvertPixToWorldCoords(FVector2D(229, 178));
	this->IcePatchLength = 2900;
	this->ice_patch_width_direction = (ConvertPixToWorldCoords(FVector2D(238,158))- 
		                               ConvertPixToWorldCoords(FVector2D(229,178))).GetSafeNormal();
	this->ice_patch_length_direction = this->ice_patch_width_direction.GetRotated(90);
	
	this->tmpTexture = LoadTextureFromPath(this->texture_location);
	this->tmpTexture->CompressionSettings = TextureCompressionSettings::TC_VectorDisplacementmap;
	this->tmpTexture->SRGB = false;
	this->tmpTexture->UpdateResource();
}

UTexture2D* ASceneSetter::LoadTextureFromPath(const FString& Path)
{
	if (Path.IsEmpty()) return NULL;
	UE_LOG(LogTemp, Warning, TEXT("NEW_CS:Success") );
    auto values = StaticLoadObject(UTexture2D::StaticClass(), NULL, *(Path));
	auto casted = Cast<UTexture2D>(values);
	return casted;
}

FVector2D ASceneSetter::ConvertPixToWorldCoords(const FVector2D& pix_coord)
{
	FVector2D world_coord;
	world_coord.X = cam_location.X + ((texture_size.X/2.0)-pix_coord.Y)*ortho_width/texture_size.X;
	world_coord.Y = cam_location.Y + (pix_coord.X-(texture_size.Y/2.0))*ortho_width/texture_size.Y;
	return world_coord;
}

FVector2D ASceneSetter::ConvertWorldToPixCoords(const FVector2D& world_coord)
{
	FVector2D pix_coord;
	pix_coord.Y = (int32)FMath::RoundFromZero((texture_size.X/2.0) - (world_coord.X-cam_location.X)*texture_size.X/ortho_width);
	pix_coord.X = (int32)FMath::RoundFromZero((world_coord.Y-cam_location.Y)*texture_size.X/ortho_width + (texture_size.X/2.0));
	return pix_coord;
}

int32 ASceneSetter::GetImageCoordFromWidth(int32 im_start, float world_width)
{
	FVector2D wc = ConvertPixToWorldCoords(FVector2D(0, im_start));
	wc.X -= world_width;
	return (int32)ConvertWorldToPixCoords(wc).Y;
}

bool ASceneSetter::SpawnTrees(int32 tree_count){
	TActorIterator<AInstancedFoliageActor> foliageIterator(GetWorld());
	AInstancedFoliageActor* foliageActor = *foliageIterator;
	TArray<UInstancedStaticMeshComponent*> components;
	foliageActor->GetComponents<UInstancedStaticMeshComponent>(components);
	UInstancedStaticMeshComponent* meshComponent;
	meshComponent = components[4];

	/*if (GEngine){	
		for(int i=0; i<components.Num(); i++){
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("ID %d, Instance Count %d"), i, components[i]->GetInstanceCount()));
		}
	}*/

	int32 trees_left = tree_count-spawnedTrees.Num();
	int spawn_count = 0;

	const FColor* FormatedImageData = static_cast<const FColor*>(this->tmpTexture->PlatformData->Mips[0].BulkData.LockReadOnly());
	// if (GEngine)
	// 		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Going to spawn %d, %d trees"), trees_left, spawnedTrees.Num()));
	while(trees_left>0){
		if (spawn_count > max_tree_spawn_per_tick) break;
		int32 rand_img_X = FMath::RandRange((int32)this->tree_topleft.X, (int32)this->tree_bottomright.X);
		int32 rand_img_Y = FMath::RandRange((int32)this->tree_topleft.Y, (int32)this->tree_bottomright.Y);
		FColor PixelColor = FormatedImageData[(int32)(rand_img_Y * this->texture_size.X + rand_img_X)];	
		if (PixelColor.R == 50 && PixelColor.G == 0 && PixelColor.B == 205) {
			auto world_coord = ConvertPixToWorldCoords(FVector2D(rand_img_X, rand_img_Y));
			FTransform transform = FTransform();				
			FCollisionQueryParams QueryParams;
			QueryParams.AddIgnoredActor(this);
			QueryParams.bTraceComplex = true;
			QueryParams.bIgnoreTouches = true;
			QueryParams.bFindInitialOverlaps=false;
			QueryParams.bIgnoreBlocks = false;

			const FVector TraceStart = FVector(world_coord.X, world_coord.Y, scan_start_height);
			const FVector TraceEnd = FVector(world_coord.X, world_coord.Y, scan_to_Z);

			FHitResult Hit;
			const bool hit_success = GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Visibility, QueryParams);

			//if (GEngine)
	 		//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, Hit.GetComponent()->GetName());

			if (hit_success && Hit.IsValidBlockingHit())
			{
				bool correct_surface = false;
				if ( Hit.GetActor()->GetClass()->GetName().Equals(FString("Landscape")) || 
					Hit.GetActor()->GetClass()->GetName().Equals(FString("FbxScene_RoadMesh_C")) ||
					Hit.GetComponent()->GetName().Equals(FString("FoliageInstancedStaticMeshComponent_3")) )
					correct_surface = true;
				if (correct_surface){
					transform.SetLocation(Hit.ImpactPoint);
					int32 instance_id = meshComponent->AddInstance(transform);
					spawnedTrees.Add(instance_id);
					trees_left--;
					spawn_count++;
				}
			}
		}
	}
	this->tmpTexture->PlatformData->Mips[0].BulkData.Unlock();
	if (trees_left == 0) return true;
	else return false;
}

void ASceneSetter::ClearScene() {
	TActorIterator<AInstancedFoliageActor> foliageIterator(GetWorld());
	AInstancedFoliageActor* foliageActor = *foliageIterator;
	TArray<UInstancedStaticMeshComponent*> components;
	foliageActor->GetComponents<UInstancedStaticMeshComponent>(components);
	components[3]->ClearInstances();
	components[4]->ClearInstances();
}

bool ASceneSetter::SpawnIcePatches(int32 ice_count){
	TActorIterator<AInstancedFoliageActor> foliageIterator(GetWorld());
	AInstancedFoliageActor* foliageActor = *foliageIterator;
	TArray<UInstancedStaticMeshComponent*> components;
	foliageActor->GetComponents<UInstancedStaticMeshComponent>(components);
	UInstancedStaticMeshComponent* meshComponent;
	meshComponent = components[3];

	int32 icep_left = ice_count-spawnedIcePatches.Num();
	int spawn_count = 0;

	// if (GEngine)
	// 		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Going to spawn %d, %d ice patches"), icep_left, spawnedIcePatches.Num()));

	while(icep_left>0){
		if (spawn_count > max_ice_spawn_per_tick) break;
		FVector2D ipc_tlc_w = FVector2D(IcePatchWidth, -IcePatchLength / 2.0);
		FVector2D ipc_brc_w = FVector2D(0, IcePatchLength / 2.0);

		float rand_W_X = FMath::RandRange((float)ipc_tlc_w.X, (float)ipc_brc_w.X);
		float rand_W_Y = FMath::RandRange((float)ipc_tlc_w.Y, (float)ipc_brc_w.Y);

		auto world_coord = this->ice_patch_center + rand_W_X * this->ice_patch_width_direction + rand_W_Y * this->ice_patch_length_direction;
		FTransform transform = FTransform();		
		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceComplex = true;
		QueryParams.bIgnoreTouches = true;
		QueryParams.bFindInitialOverlaps=false;
		QueryParams.bIgnoreBlocks = false;

		const FVector TraceStart = FVector(world_coord.X, world_coord.Y, scan_start_height);
		const FVector TraceEnd = FVector(world_coord.X, world_coord.Y, scan_to_Z);

		FHitResult Hit;
		const bool hit_success = GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Visibility, QueryParams);

		if (hit_success && Hit.IsValidBlockingHit())
		{
			bool correct_surface = true;
			/*if ( Hit.GetActor()->GetClass()->GetName().Equals(FString("Landscape")) || 
				 Hit.GetActor()->GetClass()->GetName().Equals(FString("FbxScene_RoadMesh_C"))  )
				correct_surface = true;*/
			
			if (correct_surface) {
				// Random Yaw Rotation
				FQuat Yaw = FQuat(FVector(0,0,1), FMath::RandRange((float)0, (float)6.28));
				// Random Scale
				float XYScale = FMath::RandRange((float)0.2, (float)0.6);
				float ZScale = FMath::RandRange((float)0.1, (float)0.2); 
				// Align with surface normal
				FVector UpVector(0,0,1);
				FVector RotationAxis = FVector::CrossProduct(UpVector, Hit.ImpactNormal);
				RotationAxis.Normalize();
				float DotProduct = FVector::DotProduct(UpVector, Hit.ImpactNormal);
				float RotationAngle = acosf(DotProduct);
				FQuat Quat = FQuat(RotationAxis, RotationAngle)*Yaw;

				transform.SetScale3D(FVector(XYScale, XYScale, ZScale));
				transform.SetRotation(Quat);
				transform.SetLocation(Hit.ImpactPoint);
				int32 instance_id = meshComponent->AddInstance(transform);
				spawnedIcePatches.Add(instance_id);
				icep_left = icep_left - 1;
				spawn_count++;
			}
		}
	}
	if (icep_left == 0) return true;
	else return false;
}


// Called when the game starts or when spawned
void ASceneSetter::BeginPlay()
{
	Super::BeginPlay();
	ClearScene();
}

// Called every frame
void ASceneSetter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(TriggerSpawn){
		int32 num_patches = (int32)(IceDensity * IcePatchWidth * IcePatchLength);
		if (GEngine){
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Spawning %d ice_patches and %d trees"), num_patches, TreeCount));
		}
		if (SpawnIcePatches(num_patches) && SpawnTrees(TreeCount)) delay_ticks--;
		if(delay_ticks==0)Destroy();
	}
}

void ASceneSetter::SetIceDensity(float NewIceDensity){
	IceDensity = NewIceDensity;
}

void ASceneSetter::SetIcePatchWidth(float NewIcePatchWidth){
	IcePatchWidth = NewIcePatchWidth;
}

void ASceneSetter::SetTreeCount(int NewTreeCount){
	TreeCount = NewTreeCount;
}

void ASceneSetter::SetTriggerSpawn(bool ts){
	TriggerSpawn = ts;
}

bool ASceneSetter::GetSimSpawnBusy(){
	return TriggerSpawn;
}
