#include "Carla.h"
#include "Carla/iSIM/iSIMFactory.h"

#include "Carla/iSIM/SceneSetter.h"
#include "Carla/Actor/ActorBlueprintFunctionLibrary.h"

TArray<FActorDefinition> AiSIMFactory::GetDefinitions()
{
  using ABFL = UActorBlueprintFunctionLibrary;
  auto SceneSetter = ABFL::MakeGenericDefinition(
      TEXT("isim"),
      TEXT("scenario"),
      TEXT("scenesetter"));
  SceneSetter.Class = ASceneSetter::StaticClass();

  FActorVariation IceDensity;
  IceDensity.Id = FString("ice_density");
  IceDensity.Type = EActorAttributeType::Float;
  IceDensity.RecommendedValues = { TEXT("5.0f") };
  IceDensity.bRestrictToRecommended = false;
  SceneSetter.Variations.Emplace(IceDensity);

  FActorVariation IcePatchWidth;
  IcePatchWidth.Id = FString("ice_patch_width");
  IcePatchWidth.Type = EActorAttributeType::Float;
  IcePatchWidth.RecommendedValues = { TEXT("200.0f") };
  IcePatchWidth.bRestrictToRecommended = false;
  SceneSetter.Variations.Emplace(IcePatchWidth);

  FActorVariation TreeCount;
  TreeCount.Id = FString("tree_count");
  TreeCount.Type = EActorAttributeType::Int;
  TreeCount.RecommendedValues = { TEXT("4") };
  TreeCount.bRestrictToRecommended = false;
  SceneSetter.Variations.Emplace(TreeCount);

  FActorVariation TriggerSpawn;
  TriggerSpawn.Id = FString("trigger_spawn");
  TriggerSpawn.Type = EActorAttributeType::Bool;
  TriggerSpawn.RecommendedValues = { TEXT("true") };
  TriggerSpawn.bRestrictToRecommended = false;
  SceneSetter.Variations.Emplace(TriggerSpawn);
 
  return { SceneSetter };
}

FActorSpawnResult AiSIMFactory::SpawnActor(
    const FTransform &Transform,
    const FActorDescription &Description)
{
  auto *World = GetWorld();
  if (World == nullptr)
  {
    // UE_LOG(LogCarla, Error, TEXT("AiSIMFactory: cannot spawn controller into an empty world."));
    return {};
  }

  auto *SceneSetter = World->SpawnActorDeferred<ASceneSetter>(
    Description.Class, 
    Transform, 
    this, 
    nullptr, 
    ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
  
  if (SceneSetter == nullptr)
  {
    UE_LOG(LogCarla, Error, TEXT("AiSIMFactory:: spawn SceneSetter failed."));
  }
  else
  {
    // Retrieve Params
    float IceDensity = UActorBlueprintFunctionLibrary::RetrieveActorAttributeToFloat("ice_density",
        Description.Variations,
        5.0f);
    SceneSetter->SetIceDensity(IceDensity);

    float IcePatchWidth = UActorBlueprintFunctionLibrary::RetrieveActorAttributeToFloat("ice_patch_width",
        Description.Variations,
        200.0f);
    SceneSetter->SetIcePatchWidth(IcePatchWidth);

    int TreeCount = UActorBlueprintFunctionLibrary::RetrieveActorAttributeToInt("tree_count",
        Description.Variations,
        4);
    SceneSetter->SetTreeCount(TreeCount);

    bool TriggerSpawn = UActorBlueprintFunctionLibrary::RetrieveActorAttributeToBool("trigger_spawn",
        Description.Variations,
        false);
    SceneSetter->SetTriggerSpawn(TriggerSpawn);
  }
  UGameplayStatics::FinishSpawningActor(SceneSetter, Transform);
  return FActorSpawnResult{SceneSetter};
}
