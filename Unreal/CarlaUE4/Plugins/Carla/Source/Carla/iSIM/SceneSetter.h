// Copyright (c) 2017 Computer Vision Center (CVC) at the Universitat Autonoma de Barcelona (UAB). This work is licensed under the terms of the MIT license. For a copy, see <https://opensource.org/licenses/MIT>.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InstancedFoliageActor.h"
#include "Containers/Array.h"
#include "SceneSetter.generated.h"

UCLASS()
class CARLA_API ASceneSetter : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASceneSetter();
	UFUNCTION(BlueprintCallable, Category = "Scene Params")
	void SetIceDensity(float NewIceDensity);

	UFUNCTION(BlueprintCallable, Category = "Scene Params")
	void SetIcePatchWidth(float NewIcePatchWidth);

	UFUNCTION(BlueprintCallable, Category = "Scene Params")
	void SetTreeCount(int NewTreeCount);

	UFUNCTION(BlueprintCallable, Category = "Scene Params")
	void SetTriggerSpawn(bool ts);

	UFUNCTION(BlueprintCallable, Category = "Scene Params")
	bool GetSimSpawnBusy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(EditAnywhere)
	float IceDensity = 0;

	UPROPERTY(EditAnywhere)
	float IcePatchWidth = 0;

	UPROPERTY(EditAnywhere)
	int TreeCount = 0;

	UPROPERTY(EditAnywhere)
	bool TriggerSpawn = false;

private:
	FVector cam_location;
	FVector2D tree_topleft, tree_bottomright;
	FVector2D texture_size;
	FVector2D ice_patch_width_direction, ice_patch_length_direction, ice_patch_center;
	float IcePatchLength;
	float ortho_width;
	float scan_start_height;
	float scan_to_Z;
	int32 ice_spawn_left = 0;
	int32 tree_spawn_left = 0;
	int max_tree_spawn_per_tick = 3;
	int max_ice_spawn_per_tick = 100;
	int delay_ticks = 20;
	UTexture2D* tmpTexture;
	FString texture_location = "Texture2D'/Game/iSIM/Maps/TownOffRoad_Support/SegMap_Processing.SegMap_Processing'";
	TArray<int32> spawnedTrees;
	TArray<int32> spawnedIcePatches;
	UTexture2D* LoadTextureFromPath(const FString& Path);
	FVector2D ConvertPixToWorldCoords(const FVector2D& pix_coord);
	FVector2D ConvertWorldToPixCoords(const FVector2D& world_coord);
	bool SpawnTrees(int32 tree_count);
	bool SpawnIcePatches(int32 ice_count);
	int32 GetImageCoordFromWidth(int32 im_start, float world_width);
	void ClearScene();
};
